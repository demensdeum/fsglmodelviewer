#include <ctime>
#include <iostream>
#include <memory>
#include <SDL2/SDL.h>
#include <FSGL/Controller/FSGLController.h>
#include <FlameSteelEngineGameToolkit/Data/Components/FSEGTFactory.h>
#include <FlameSteelEngineGameToolkitFSGL/Data/FSGTIOFSGLSystemFactory.h>

using namespace std;

int main(int argc, char *argv[])
{
	cout << "FSGL Model Viewer" << endl;

	if (argc != 2) {
		cout << "Usage: fsglmodelviewer model.fsgl" << endl;
		return 1;
	}

	shared_ptr<string> modelPath = make_shared<string>(argv[1]);

    auto ioSystemParams = make_shared<FSEGTIOGenericSystemParams>();
    ioSystemParams->title = make_shared<string>("FSGL: Render Model Test");
    ioSystemParams->width = 640;
    ioSystemParams->height = 480;

	auto controller = make_shared<FSGLController>();
      	controller->initialize(ioSystemParams);

	auto object = FSEGTFactory::makeOnSceneObject(
														            make_shared<string>("Model"),
														            make_shared<string>("Model"),
														            shared_ptr<string>(),
														            modelPath,
                                                                    					     shared_ptr<string>(),
														            -0.85, -0.4, -2.2,
														            1, 1, 1,
														            0, -0.6, 0,
														            0);	

	auto materialLibrary = make_shared<MaterialLibrary>();

	auto graphicsObject = FSGTIOFSGLSystemFactory::graphicsObjectFrom(object, materialLibrary);

	if (graphicsObject.get() == nullptr) {
		throw logic_error("graphics object is null, test failed");
	}

	controller->addObject(graphicsObject);
    
    auto startTime = time(nullptr);
    
	SDL_Event event;
    while(time(nullptr) - startTime < 3) {
        SDL_PollEvent(&event);
        controller->render();    
    }

	return 0;
}
